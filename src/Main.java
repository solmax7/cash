import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

class Lru {

//    public static void main(String[] args) throws IOException {
//
//        System.out.println("Выберите действие!!! 1. Добавить запись в кеш.  2. Удалить запись. 3. Показать все записи");
//        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
//
//        String number = reader.readLine();
//        //String sAge = reader.readLine();
//        int nnumber= Integer.parseInt(number);
//
//        switch (nnumber) {
//            case  (1):
//                System.out.println("Вы выбрали 1");
//                break;
//            case (2):
//                System.out.println("Вы выбрали 2");;
//                break;
//            case (3):
//                System.out.println("Вы выбрали 3");;
//                break;
//            default:
//                System.out.println("Вы выбрали неправильное число!!!");;
//                break;
//        }
//
//        Map<String, String> cash = new HashMap<>();
//
//        cash.put("1", "Вася");
//
//        Iterator<Map.Entry<String, String>> itr = cash.entrySet().iterator();
//        while (itr.hasNext())
//            System.out.println(itr.next());


    public static <K,V> Map<K,V> lruCache(final int maxSize) {
        return new LinkedHashMap<K, V>(maxSize*4/3, 0.75f, true) {

            @Override
            protected boolean removeEldestEntry(Map.Entry<K, V> eldest) {
                return size() > maxSize;
            }
        };
    }

    public static void main(String[] args ) {
        Map<Object, Object> lru = Lru.lruCache(3);
        lru.put("2", "5");
        lru.put("1", "2");
        lru.put("1", "1");
        lru.put("2", "2");
        lru.put("3", "3");
        System.out.println(lru);

        LFUCache lfu = new LFUCache(5);

        //lfu.getCacheEntry(5);

        lfu.addCacheEntry(1, "Вася");
        lfu.addCacheEntry(2, "Петя");
        lfu.addCacheEntry(3, "Ваня");
        lfu.addCacheEntry(4, "Мася");
        lfu.addCacheEntry(5, "Пуся");

        lfu.getCacheEntry(5);
        lfu.getCacheEntry(5);
        lfu.getCacheEntry(4);
        lfu.getCacheEntry(3);
        lfu.getCacheEntry(1);

        lfu.showCache();

        lfu.addCacheEntry(100, "Тест");




        lfu.showCache();




    }
}

